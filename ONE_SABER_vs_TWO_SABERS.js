in_array = function (needle, haystack, strict) 
{   
	// Checks if a value exists in an array
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    var found = false, key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}

var two_swords = []
var one_sword = []

var one_sword_damage = 0
var two_swords_damage = 0

var one_sword_passed_strikes = 0
var two_swords_passed_strikes = 0

var one_sword_passed_blocks = 0
var two_swords_passed_blocks = 0

var count = 1000
var damage = 50

for (var i=0;i<count;i++){
	var two_swords_turn = {a:[Math.floor(Math.random() * 3 +1),Math.floor(Math.random() * 3 +1)],d:[Math.floor(Math.random() * 3 +1)]}
	var one_swords_turn = {a:[Math.floor(Math.random() * 3 +1)],d:[Math.floor(Math.random() * 3 +1),Math.floor(Math.random() * 3 +1)]}
	
	two_swords.push(two_swords_turn)
	one_sword.push(one_swords_turn)
	
	
	for(var j in two_swords_turn.a){
		if ( !in_array(two_swords_turn.a[j], one_swords_turn.d) ){
			// console.log(i,"Two swords make "+j+" attack: ", "a: "+two_swords_turn.a[j], "d: "+one_swords_turn.d[0]);
			two_swords_damage += damage;
			two_swords_passed_strikes += 1
		}else{
			one_sword_passed_blocks += 1
		}
	}
	

	if ( one_swords_turn.a[0] != two_swords_turn.d[0] ){
		// console.log(i,"Two swords make "+j+" attack: ", "a: "+two_swords_turn.a[j], "d: "+one_swords_turn.d[0]);
		one_sword_damage += damage;
		one_sword_passed_strikes += 1
	}else{
		two_swords_passed_blocks += 1
	}
}
console.log("one_sword_passed_strikes: "+one_sword_passed_strikes,"two_swords_passed_strikes: "+two_swords_passed_strikes)
console.log("one_sword_passed_blocks: "+one_sword_passed_blocks,"two_swords_passed_blocks: "+two_swords_passed_blocks)
console.log("one_sword_damage: "+one_sword_damage,"two_swords_damage: "+two_swords_damage)