GameDesignClass = class();

function GameDesignClass:create_rect()
	
	local lightsaber = display.newImage(  "img/lightsaber_red.png", true )
	lightsaber.id = 'OLOLO'; 
	lightsaber:setReferencePoint( display.BottomCenterReferencePoint );
	local koef = lightsaber.contentWidth / lightsaber.contentHeight;
	lightsaber.xScale = _H*0.4*koef / lightsaber.contentWidth
	lightsaber.yScale = _H*0.4 / lightsaber.contentHeight
	lightsaber.x = _W/2
	lightsaber.y = _H/2
	

	return lightsaber
end

--[[
	params = [
		name 			- str, name of object,
		group			- obj, group to inserting
		referencePoint 	- obj, referrence point for obj
		x 				- round, x coordinate of obj
		y				- round, y coordinate of obj
		alpha			- round, alpha of obj,
	]
]]
function GameDesignClass:create_image(params)
	local image
	
	image = display.newImage( params.name, _IMAGES_DERICTORY, true );

-- REFERENCE POINT	
	if not params.referencePoint then
		params.referencePoint = display.CenterReferencePoint;
	end
	
	image:setReferencePoint( params.referencePoint );

-- GROUP INSERTING	
	if params.group then
		params.group:insert(image)
	end
	
-- X,Y COORDINATES	
	image.x = params.x or 0;
	image.y = params.y or 0;
	
-- ALPHA	
	image.alpha = params.alpha or 1;

-- LISTENER	
	if params.listener then
		for i,v in pairs(params.listener) do
			image.listener = i;
			image[i] = v;
		end
	end

-- SCALE
	if not params.scale then
		params.scale = image.contentWidth;
		params.scaleType = "x"
	end
	
	local koefX = image.contentWidth / image.contentHeight;
	local koefY = image.contentHeight / image.contentWidth;
	
	local scaleX = params.scale;
	local scaleY = params.scale;
	
	if params.scaleType == "x" then
		koefX = 1;		
	else
		koefY = 1;
	end
	
	if params.scaleX then
		scaleX = params.scaleX;
		koefX = 1;
	end
	
	if params.scaleY then
		scaleY = params.scaleY;
		koefY = 1;
	end	
	
	image.xScale = scaleX*koefX / image.contentWidth
	image.yScale = scaleY*koefY / image.contentHeight

-- RESULT
	return image
end

function GameDesignClass:create_main_menu()
	local result = {}
	result.group = display.newGroup();
	result.bg = display.newRect(result.group, 0, 0, _W, _H)
	result.bg:setFillColor(63, 47, 47)
	
	result.lightsaber = self:create_image({name="img/lightsaber_red.png", group = result.group,x = _W/2,y = _H/2, referencePoint=display.BottomCenterReferencePoint, scale = _H*0.4, scaleType = "y"});
	
	result.bttnPlay = display.newRect(result.group, 0, 0, _W/6, _W/12);
	result.bttnPlay:setFillColor(0, 133, 0)
	result.bttnPlay:setReferencePoint( display.CenterRightReferencePoint );
	result.bttnPlay.x = _W/2;
	result.bttnPlay.y = _H/2 + result.bttnPlay.contentHeight/1.9;
	
	result.bttnExit = display.newRect(result.group, 0, 0, _W/6, _W/12);
	result.bttnExit:setFillColor(133, 0, 0)
	result.bttnExit:setReferencePoint( display.CenterLeftReferencePoint );
	result.bttnExit.x = _W/2;
	result.bttnExit.y = _H/2 + result.bttnExit.contentHeight/1.9;	

	return result
end

function GameDesignClass:create_button()
	local result = {}
	result.button = display.newRect(0, 0, _W/2, _H/2)
	result.button:setFillColor(0)
	print("create_button")
	print_r(self)
	return result.button;
end

function GameDesignClass:create_test()
	local result = {}
	result.test = display.newRect(0, 0, _W/2, _H/2)
	result.test:setFillColor(0)
	print("create_test")
	print_r(self)
	return result.test;
end