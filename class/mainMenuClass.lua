mainMenuClass = class();

function mainMenuClass:create(params)
	--	CREATE display objects and add them to 'group' here.
	--	ALL display objects IS IN self.interface property of class
	
	self.interface = GDC:create_main_menu();
	
	if params.func then
		self.interface.bttnPlay:addEventListener("tap", params.func)
	end
end

function mainMenuClass:remove()
	--	REMOVE display objects and other properties of the class here.
	
	for i,v in pairs(self) do
		display.remove(self[i])
		self[i] = nil
	end
	
end