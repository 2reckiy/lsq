testClass = class();

function testClass:create()
	-- self.value = 15;
	self.name = "Max"
	self.interface = GDC:create_rect();
end

function testClass:remove()
	
	
	for i,v in pairs(self) do
	print("self[i]",type(self[i]),self[i])
		display.remove(self[i])
		self[i] = nil
	end
	
end


--[[
	
Animal = class(function(a,name)
   a.name = name
end)

function Animal:__tostring()
  return self.name..': '..self:speak()
end

Dog = class(Animal)

function Dog:speak()
  return 'bark'
end

Cat = class(Animal, function(c,name,breed)
         Animal.init(c,name)  -- must init base!
         c.breed = breed
      end)

function Cat:speak()
  return 'meow'
end
function Cat:breeding()
  return self.breed;
end

Lion = class(Cat)

function Lion:speak()
  return 'roar'
end

fido = Dog('Fido')
felix = Cat('Felix','Tabby')
leo = Lion('Leo','African')

print(fido,felix,leo)
print(leo:breeding())

--]]