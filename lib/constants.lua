--------------------
--
-- GLOBAL CONSTATNTS
--
--------------------

-- HEIGHT AND WIDTH OF DEVICE 
	_W = display.viewableContentWidth  + -1 * display.screenOriginX;
	_H = display.viewableContentHeight + -1 * display.screenOriginY;
	
-- Имя устройства на котором запускается приложение
	-- Info from http://developer.anscamobile.com/reference/index/systemgetinfo
	local arch = system.getInfo ("platformName")
	_DEVICE = "apple"
	if (string.find (arch, "iPhone OS") ~= nil) then
		-- Apple
		_DEVICE = "apple";
	elseif (string.find (arch, "Android") ~= nil) then
		-- Android
		_DEVICE = "android";
	elseif ( string.find (arch, "Mac OS X") ~= nil ) then
		-- MacOS
		_DEVICE = "mac";
	else
		-- Windows
		_DEVICE = "win";
		-- No supported yet by Corona
	end
	
-- IMAGES DERICTORY FOR DEVICES
	if (_DEVICE == "android1") then
		-- _IMAGES_DERICTORY = system.pathForFile( "", system.CachesDirectory );
		_IMAGES_DERICTORY = system.CachesDirectory;
	else
		-- _IMAGES_DERICTORY = system.pathForFile( "", system.ResourceDirectory );
		_IMAGES_DERICTORY = system.ResourceDirectory;
	end	