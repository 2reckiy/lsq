module(..., package.seeall)
PerformanceOutput = {};
PerformanceOutput.mt = {};
PerformanceOutput.mt.__index = PerformanceOutput;
PerformanceOutput.indicator = true;
 
 
local prevTime = 0;
local maxSavedFps = 30;
 
function createLayout(self)
        local group = display.newGroup();
        local background = display.newRect(-50,0, 240, 100);
        background:setReferencePoint (display.TopLeftReferencePoint)
        background.x = 0
        background.y = 0
        background:setFillColor(0,0,0);
        group:insert(background);
        
        self.memory = display.newText 		(group, "0/10", 80,  0,                  "Helvetica", 28);
        self.lmem = display.newText			(group, "0",    100, self.memory.height, "Helvetica", 28);
        self.framerate = display.newText	(group, "0",    100, self.memory.height + self.lmem.height, "Helvetica", 28);
        
        self.memory:setTextColor(255,255,255);
        self.framerate:setTextColor(255,255,255);
        self.lmem:setTextColor(255,255,255);

		group.alpha = 0.7
		group:setReferencePoint (display.setReferencePoint)

		group.x, group.y = 100,  20;
		return group;
end
 
local function minElement(table)
        local min = 10000;
        for i = 1, #table do
                if(table[i] < min) then min = table[i]; end
        end
        return min;
end
 
 
local function getLabelUpdater(self)
        local lastFps = {};
        local lastFpsCounter = 1;

        return function(event)
                local curTime = system.getTimer();
                local dt = curTime - prevTime;
                prevTime = curTime;
        
                local fps = math.floor(1000/dt);
                
                lastFps[lastFpsCounter] = fps;
                lastFpsCounter = lastFpsCounter + 1;
                if(lastFpsCounter > maxSavedFps) then lastFpsCounter = 1; end
                
                local minLastFps = minElement(lastFps); 
                
                self.framerate.text = "FPS: "..fps.."(min: "..minLastFps..")";
                self.memory.text 	= "TMem: "..(roundTo (system.getInfo("textureMemoryUsed")/ (1024*1024), 0)).." mb";
                self.lmem.text	 	= "LMem: "..(roundTo (collectgarbage("count") / 1024)).." mb";

				if self.indicator then
					self.group:toFront ()
				end
        end
end
 
 
local instance = nil;
-- Singleton
function PerformanceOutput.new()
        if(instance ~= nil) then return instance; end
        local self = {};
        setmetatable(self, PerformanceOutput.mt);
        
        self.group = createLayout(self);

        
        self.indicator = true;
        Runtime:addEventListener("enterFrame", getLabelUpdater(self));
        instance = self;
		
        return self;
end