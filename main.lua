-- hide device status bar
	display.setStatusBar( display.HiddenStatusBar )

-- CONSTANTS AND VERIABLES
	require 'lib.constants';
-- library loading
	require 'class'; -- located in the ../Corona_Directory/Resources
	require 'lib.displayex';
	require 'lib.lib';
	require 'lib.mathlib';
-- classes loading	
	require 'class.gameDesignClass';	
	require 'class.testClass';	
	require 'class.headerClass';	
	require 'class.mainMenuClass';	
	
-- set library to GLOBAL variable
	widget = require "widget"

-- require controller module
	storyboard = require( "storyboard" )
	
	storyboard.isDebug = false;	
	Runtime:addEventListener( "enterFrame", storyboard.printMemUsage ); -- print mem usage info on every frame
	
-- INIT CLASSES
	GDC = GameDesignClass();

-- load first scene
	storyboard.gotoScene( "scene.developer", "crossFade", 1000 )

	
--[[ _DEBUG PROPERTIES AND FUNCTIONS ]]
	-- stat = require("lib.stat");
	-- performance = stat.PerformanceOutput.new();

	
	
	
	-- headerM = headerClass();	
	-- headerM:create();