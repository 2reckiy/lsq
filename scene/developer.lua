---------------------------------------------------------------------------------
--
-- scene1.lua
--
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local image, text1, text2, text3, memTimer
-- local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
-- Touch event listener for background image
local function onSceneTouch( self, event )
	if event.phase == "began" then
		storyboard.gotoScene( "scene.game_logo", "crossFade", 1000  )
		
		return true
	end
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local screenGroup = self.view
	
	image = GDC:create_image({name="img/developer.png", group=screenGroup, alpha = 0, x = _W/2, y = _H/2, listener = {touch = onSceneTouch}});
	-- image.touch = onSceneTouch

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	
	print( "1: enterScene event" )
	-- remove previous scene's view
	-- storyboard.purgeScene( "scene2" )
	transition.to(image, {time=1000, alpha=1})
	image:addEventListener( image.listener, image )
	
	local showMem = function()
		storyboard.gotoScene( "scene.game_logo", "crossFade", 1000  )
	end
	memTimer = timer.performWithDelay( 2000, showMem, 1 )	

end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	image:removeEventListener( image.listener, image )
	
	-- cancel timer
	timer.cancel( memTimer ); memTimer = nil;	
	print( "1: exitScene event" )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
	print( "((destroying scene 1's view))" )
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene