
local scene = storyboard.newScene()
 
----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
local gemsCount = {0,0,0,0}
local mRandom = math.random

local score
local scoreText

local gameTimer
local gameTimerText
local gameTimerBar
local gameTimerBar_bg


local gemsTable = {}
local possibleGemsTable = {}
local markedToDestroy = {v={},h={}}

local numberOfMarkedToDestroy = 0
local gemToBeDestroyed  			-- used as a placeholder
local isGemTouchEnabled = true 		-- blocker for double touching gems

local gameOverLayout
local gameOverText1
local gameOverText2

local timers = {}

-- pre-declaration of function
local onGemTouch,dragGem



local function setPossibleGem()
    for i = 1, 8, 1 do

    	possibleGemsTable[i] = {}

		for j = 1, 8, 1 do
			possibleGemsTable[i][j] = {1,2,3,4};
 		end
 	end
end


local function checkRow(i,j)
	local beforeGemLeft,beforeGemUp
	local nowGem
	local possibleGemsRight = {1,2,3,4}
	local possibleGemsBottom = {1,2,3,4}
	
	if possibleGemsTable[i-1] then
		beforeGemLeft = possibleGemsTable[i-1][j][1];
	end
	if possibleGemsTable[i][j-1] then
		beforeGemUp = possibleGemsTable[i][j-1][1];
	end	
	nowGem = possibleGemsTable[i][j][1];
	
	if beforeGemLeft == nowGem then
		if possibleGemsTable[i+1] then
			table.remove(possibleGemsRight,nowGem)
			possibleGemsTable[i+1][j] = possibleGemsRight
		end
	end
	
	if beforeGemUp == nowGem then
		if possibleGemsTable[i][j+1] then
			table.remove(possibleGemsBottom,nowGem)
			possibleGemsTable[i][j+1] = possibleGemsBottom
		end
	end
end


local function createGem(i,j)
	local whatGems = possibleGemsTable[i][j];
	local gem;

	gem = whatGems[mRandom(1,#whatGems)];
	
	possibleGemsTable[i][j] = {gem};
	
	checkRow(i,j)

	return gem;
end


local function newGem (i,j)

	local newGem
	newGem = display.newCircle((i-1)*40, -60, 20)
	newGem.i = i
	newGem.j = j
	newGem.isMarkedToDestroy = false

	newGem.destination_y = (j-1)*40

	local R = createGem(i,j);

	
	
	if (R == 1 ) then 
	
		newGem:setFillColor( 200, 0,   0 )
		newGem.gemType = "red"
	
	elseif (R == 2 ) then 
	
		newGem:setFillColor( 0, 200, 0 )
		newGem.gemType = "green"
	
	elseif (R == 3 ) then 
	
		newGem:setFillColor( 0, 0, 200 )
		newGem.gemType = "blue"
	
	elseif (R == 4 ) then 
	
		newGem:setFillColor( 200, 200, 0 )
		newGem.gemType = "yellow"
	
	end

	--new gem falling animation
	transition.to( newGem, { time=100, y= newGem.destination_y} )
	
	--remember the initial location
	newGem.xOrig = newGem.x
	newGem.yOrig = newGem.destination_y

	groupGameLayer:insert( newGem )


	-- newGem.touch = onGemTouch
	newGem.touch = dragGem
	newGem:addEventListener( "touch", newGem )

return newGem
end


local function shiftGems () -- not working yet <-- you stupid.. oh u <_< of coz it's working! :P

print ("Shifting Gems")

	-- first roww
	for i = 1, 8, 1 do
			if gemsTable[i][1].isMarkedToDestroy then

					-- current gem must go to a 'gemToBeDestroyed' variable holder to prevent memory leaks
					-- cannot destroy it now as gemsTable will be sorted and elements moved down
					gemToBeDestroyed = gemsTable[i][1]
					
					-- create a new one
					gemsTable[i][1] = newGem(i,1)
					
					-- destroy old gem
					gemToBeDestroyed:removeSelf()
					gemToBeDestroyed = nil
			end
	end

	-- rest of the rows
	for j = 2, 8, 1 do  -- j = row number - need to do like this it needs to be checked row by row
		for i = 1, 8, 1 do
			
			if gemsTable[i][j].isMarkedToDestroy then --if you find and empty hole then shift down all gems in column

					gemToBeDestroyed = gemsTable[i][j]
				
					-- shiftin whole column down, element by element in one column
					for k = j, 2, -1 do -- starting from bottom - finishing at the second row

						-- curent markedToDestroy Gem is replaced by the one above in the gemsTable
						gemsTable[i][k] = gemsTable[i][k-1]
						gemsTable[i][k].destination_y = gemsTable[i][k].destination_y +40
						transition.to( gemsTable[i][k], { time=100, y= gemsTable[i][k].destination_y} )
						
						-- we change its j value as it has been 'moved down' in the gemsTable
						gemsTable[i][k].j = gemsTable[i][k].j + 1
				
					end

					-- create a new gem at the first row as there is en ampty space due to gems
					-- that have been moved in the column
					gemsTable[i][1] = newGem(i,1)

					-- destroy the old gem (the one that was invisible and placed in gemToBeDestroyed holder)
					gemToBeDestroyed:removeSelf()
					gemToBeDestroyed = nil
			end 
		end
	end
end --shiftGems()


local function markToDestroy( self )

	if self.direction then
		table.insert(markedToDestroy[self.direction] , {self.i,self.j});
	else
		table.insert(markedToDestroy.h , {self.i,self.j});
		table.insert(markedToDestroy.v , {self.i,self.j});
	end
	
	self.isMarkedToDestroy = true
	-- numberOfMarkedToDestroy = numberOfMarkedToDestroy + 1
	self.alpha = .5

	-- check on the left
	if self.i>1 and ( self.direction == nil or self.direction == 'h' ) then
		if (gemsTable[self.i-1][self.j]).isMarkedToDestroy == false then

			if (gemsTable[self.i-1][self.j]).gemType == self.gemType then
				gemsTable[self.i-1][self.j].direction = 'h';
				markToDestroy( gemsTable[self.i-1][self.j] )
			end	 
		end
	end

	-- check on the right
	if self.i<8 and ( self.direction == nil or self.direction == 'h' ) then
		if (gemsTable[self.i+1][self.j]).isMarkedToDestroy == false then

			if (gemsTable[self.i+1][self.j]).gemType == self.gemType then
				gemsTable[self.i+1][self.j].direction = 'h';
			    markToDestroy( gemsTable[self.i+1][self.j] )
			end	 
		end
	end

	-- check above
	if self.j>1 and ( self.direction == nil or self.direction == 'v' ) then
		if (gemsTable[self.i][self.j-1]).isMarkedToDestroy == false then

			if (gemsTable[self.i][self.j-1]).gemType == self.gemType then
				gemsTable[self.i][self.j-1].direction = 'v';
				markToDestroy( gemsTable[self.i][self.j-1] )
			end	 
		end
	end

	-- check below
	if self.j<8 and ( self.direction == nil or self.direction == 'v' ) then
		if (gemsTable[self.i][self.j+1]).isMarkedToDestroy== false then

			if (gemsTable[self.i][self.j+1]).gemType == self.gemType then
				gemsTable[self.i][self.j+1].direction = 'v';
				markToDestroy( gemsTable[self.i][self.j+1] )
			end	 
		end
	end
end


local function enableGemTouch()

	isGemTouchEnabled = true
end


local function destroyGems()
	print ("Destroying Gems. Marked to Destroy = "..numberOfMarkedToDestroy)


	for i = 1, 8, 1 do
		for j = 1, 8, 1 do
			
			if gemsTable[i][j].isMarkedToDestroy then

				isGemTouchEnabled = false
				transition.to( gemsTable[i][j], { time=300, alpha=0.2, xScale=2, yScale = 2, onComplete=enableGemTouch } )

				-- update score
				score = score + 10
				scoreText.text = string.format( "SCORE: %6.0f", score )
				scoreText:setReferencePoint(display.TopLeftReferencePoint)
				scoreText.x = 60
				
			end
		end
	end

	numberOfMarkedToDestroy = 0
	timer.performWithDelay( 320, shiftGems )

end


local function cleanUpGems()
	print("Cleaning Up Gems")
		
	numberOfMarkedToDestroy = 0
	
	for i = 1, 8, 1 do
		for j = 1, 8, 1 do
			
			-- show that there is not enough
			if gemsTable[i][j].isMarkedToDestroy then
				transition.to( gemsTable[i][j], { time=100, xScale=1.2, yScale = 1.2 } )
				transition.to( gemsTable[i][j], { time=100, delay=100, xScale=1.0, yScale = 1.0} )
			end

			gemsTable[i][j].isMarkedToDestroy = false
			

		end
	end
end

local function checkMarkedGems()
	print("Check Marked Gems")
		
	numberOfMarkedToDestroy = 0
	
	markedToDestroy
end


function onGemTouch( self, event )	-- was pre-declared

	if event.phase == "began" and isGemTouchEnabled then

		print("Gem touched i= "..self.i.." j= "..self.j)

		markToDestroy(self)
		
		if numberOfMarkedToDestroy >= 3 then

			destroyGems()
		else 
			cleanUpGems()
		end
	end

return true

end


function onTouchGameOverScreen ( self, event )

	if event.phase == "began" then

		storyboard.gotoScene( "scene.main_menu", {
													effect = "fade",
													time = 400,
													params =
													{
														from = "scene.match3"
													}
												});
		
		return true
	end
end	


local function showGameOver ()

	gameOverLayout.alpha = 0.8
	gameOverText1.alpha = 1
	gameOverText2.alpha = 1


end


local function gameTimerUpdate ()

	gameTimer = gameTimer - 1
	
	if gameTimer >= 0 then gameTimerText.text = gameTimer

	else
		gameOverText2.text = string.format( "SCORE: %6.0f", score )
		showGameOver()

	end
end



--[[
--
--
---------------------------------------------  DRAG AND DROP ---------------------------------------------------------------
--
--
--]]

--circle-based collision detection
local function hasCollidedCircle( obj1, obj2 )
   if ( obj1 == nil ) then  --make sure the first object exists
      return false
   end
   if ( obj2 == nil ) then  --make sure the other object exists
      return false
   end

   local dx = obj1.x - obj2.x
   local dy = obj1.y - obj2.y

   local distance = math.sqrt( dx*dx + dy*dy )
   local objectSize = (obj2.contentWidth/2) + (obj1.contentWidth/2)
   if ( distance < objectSize ) then
      return true
   end
   return false
end

--rectangle-based collision detection
local function changeGamesPlaces( obj1, obj2 )
	transition.to( obj1, {time=200, x=obj2.xOrig, y=obj2.yOrig} )
	transition.to( obj2, {time=200, x=obj1.xOrig, y=obj1.yOrig} )
	local x,y,i1,y1,i2,j2
	i1 = obj1.i
	j1 = obj1.j	
	i2 = obj2.i
	j2 = obj2.j
	x = obj2.xOrig
	y = obj2.yOrig
	obj2.xOrig = obj1.xOrig
	obj2.yOrig = obj1.yOrig
	obj1.xOrig = x
	obj1.yOrig = y
	obj2.i = i1
	obj2.j = j1
	obj1.i = i2
	obj1.j = j2
	gemsTable[i1][j1] = obj2
	gemsTable[i2][j2] = obj1
end

-- CHANGE PLACES OF GEMS
local function hasCollided( obj1, obj2 )
   if ( obj1 == nil ) then  --make sure the first object exists
      return false
   end
   if ( obj2 == nil ) then  --make sure the other object exists
      return false
   end

   local left = obj1.contentBounds.xMin <= obj2.contentBounds.xMin and obj1.contentBounds.xMax >= obj2.contentBounds.xMin
   local right = obj1.contentBounds.xMin >= obj2.contentBounds.xMin and obj1.contentBounds.xMin <= obj2.contentBounds.xMax
   local up = obj1.contentBounds.yMin <= obj2.contentBounds.yMin and obj1.contentBounds.yMax >= obj2.contentBounds.yMin
   local down = obj1.contentBounds.yMin >= obj2.contentBounds.yMin and obj1.contentBounds.yMin <= obj2.contentBounds.yMax

   return (left or right) and (up or down)
end


function dragGem( self, event )
	local target = event.target 
	local phase = event.phase

	if ( event.phase == "began" ) then
      local parent = target.parent
      display.getCurrentStage():setFocus( target ) 
      target.isFocus = true
      target.x0 = event.x - target.x
      target.y0 = event.y - target.y
      target.xStart = target.x
      target.yStart = target.y
      target.alpha = .5
      target:toFront()
	elseif ( target.isFocus ) then
		if ( phase == "moved" ) then
			target.x = event.x - target.x0
			target.y = event.y - target.y0
			
			-- for i=-1,1,1 do
				-- for j=-1,1,1 do
				
					-- if ( i ~= 0 and j == 0 ) or ( i == 0 and j ~= 0 ) then				
						-- if (gemsTable[target.i + i] and gemsTable[target.i + i][target.j + j] and hasCollidedCircle( target, gemsTable[target.i + i][target.j + j] ) ) then
							-- local x,y
							-- if target.xOrig == gemsTable[target.i + i][target.j + j].x then
								-- x = gemsTable[target.i + i][target.j + j].xOrig
							-- else
								-- x = target.xOrig
							-- end
							-- if target.yOrig == gemsTable[target.i + i][target.j + j].y then
								-- y = gemsTable[target.i + i][target.j + j].yOrig
							-- else
								-- y = target.yOrig
							-- end							
							-- transition.to( gemsTable[target.i + i][target.j + j], {time=1, x=x, y=y} )

						-- end

					-- end	
				-- end
			-- end				
		elseif ( phase == "ended" or phase == "cancelled" ) then
			display.getCurrentStage():setFocus( nil )
			target.isFocus = false
			target.alpha = 1
			target:setFillColor( 255, 255, 255 )
			local forEnd = false;
			for i=-1,1,1 do
				forEnd = false
				for j=-1,1,1 do
				
					if ( i ~= 0 and j == 0 ) or ( i == 0 and j ~= 0 ) then				
						if (gemsTable[target.i + i] and gemsTable[target.i + i][target.j + j] and hasCollidedCircle( event.target, gemsTable[target.i + i][target.j + j] ) ) then
							--snap in place
							changeGamesPlaces(event.target,gemsTable[target.i + i][target.j + j]);
							forEnd = true;
							break
						end

					end	
				end
				if forEnd then					
					break
				end
			end	

			if not forEnd then
				--move back
				transition.to( event.target, {time=100, x=event.target.xOrig, y=event.target.yOrig} )
			end				
			if forEnd then
				markToDestroy(self);
				-- markToDestroy(self);
				checkMarkedGems()
				print_r(markedToDestroy)
			end
		end
	end
	return true
end

--[[
--
--
-------------------------------------------------------------------------------------------------------------------------------
--
--
--]]




-- Called when the scene's view does not exist:
function scene:createScene( event )
        local screenGroup = self.view

		print( "\n1: createScene event")
 
        -----------------------------------------------------------------------------
                
        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.
        
        -----------------------------------------------------------------------------

    -- reseting values
    score		= 0
    gameTimer 	= 1000


   	groupGameLayer = display.newGroup()
   	groupEndGameLayer = display.newGroup()

    --score text

    scoreText = display.newText( "SCORE:" , 40, 20, "Helvetica", 32 )
	scoreText.text = string.format( "SCORE: %6.0f", score )
	scoreText:setReferencePoint(display.TopLeftReferencePoint)
	scoreText.x = 60
	scoreText:setTextColor(0, 0, 255)
	
	
	gameTimerBar_bg = display.newRoundedRect( 20, 430, 280, 20, 4)
	gameTimerBar_bg:setFillColor( 60, 60, 60 )
 	gameTimerBar = display.newRoundedRect( 20, 430, 280, 20, 4)
 	gameTimerBar:setFillColor( 0, 150, 0 )
 	gameTimerBar:setReferencePoint(display.TopLeftReferencePoint)

 	
 	gameTimerText = display.newText( gameTimer , 0, 0, "Helvetica", 18 )
 	gameTimerText:setTextColor( 255, 255, 255 )
	gameTimerText:setReferencePoint(display.TopLeftReferencePoint)
    gameTimerText.x = _W * 0.5 - 12
    gameTimerText.y = 426
    
	-- groupGameLayer:insert ( scoreText )
	-- groupGameLayer:insert ( gameTimerBar_bg )
 	-- groupGameLayer:insert ( gameTimerBar )
 	-- groupGameLayer:insert ( gameTimerText )
	
	-- SET POSSIBLE GAME MATRIX
	setPossibleGem();
	
    --gemsTable
    for i = 1, 8, 1 do

    	gemsTable[i] = {}

		for j = 1, 8, 1 do
			gemsTable[i][j] = newGem(i,j)
 		end
 	end
 		

 	gameOverLayout = display.newRect( 0, 0, 320, 480)
 	gameOverLayout:setFillColor( 120, 0, 120 )
 	gameOverLayout.alpha = 0
 	
 	gameOverText1 = display.newText( "GAME OVER", 0, 0, native.systemFontBold, 24 )
	gameOverText1:setTextColor( 255 )
	gameOverText1:setReferencePoint( display.CenterReferencePoint )
	gameOverText1.x, gameOverText1.y = _W * 0.5, _H * 0.5 -150
	gameOverText1.alpha = 0

	gameOverText2 = display.newText( "SCORE: ", 0, 0, native.systemFontBold, 24 )
	gameOverText2.text = string.format( "SCORE: %6.0f", score )
	gameOverText2:setTextColor( 255 )
	gameOverText2:setReferencePoint( display.CenterReferencePoint )
	gameOverText2.x, gameOverText2.y = _W * 0.5, _H * 0.5 - 50
	gameOverText2.alpha = 0

	
	gameOverLayout.touch = onTouchGameOverScreen
	gameOverLayout:addEventListener( "touch", gameOverLayout )


	groupEndGameLayer:insert ( gameOverLayout )
	groupEndGameLayer:insert ( gameOverText1 )
	groupEndGameLayer:insert ( gameOverText2 )

   	groupGameLayer.x = _W/2 - 320/2
   	groupGameLayer.y = _H/2 - 320/2
	
	-- insterting display groups to the screen group (storyboard group)
	screenGroup:insert ( groupGameLayer )
	screenGroup:insert ( groupEndGameLayer )
end
 
-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local screenGroup = self.view
        
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.782 or later.
        
        -----------------------------------------------------------------------------
        
end
 
-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local screenGroup = self.view
        
        -----------------------------------------------------------------------------
                
        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)
        
        -----------------------------------------------------------------------------
	
	-- remove previous scene's view
        
    storyboard.purgeScene( "scene.main-menu" )

	-- reseting values
	score = 0
	

	transition.to( gameTimerBar, { time = gameTimer * 1000, width=0 } )
	timers.gameTimerUpdate = timer.performWithDelay(1000, gameTimerUpdate, 0)
				


end
  
-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local screenGroup = self.view
        
        -----------------------------------------------------------------------------
        
        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
        
        -----------------------------------------------------------------------------
       
	if timers.gameTimerUpdate then 
		timer.cancel(timers.gameTimerUpdate)
		timers.gameTimerUpdate = nil
	 end

end
 
-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local screenGroup = self.view
        
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.782 or later.
        
        -----------------------------------------------------------------------------
        
end
 
 -- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        local screenGroup = self.view
        
        -----------------------------------------------------------------------------
        
        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)
        
        -----------------------------------------------------------------------------
        
end
  
-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local screenGroup = self.view
        local overlay_scene = event.sceneName  -- overlay scene name
        
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.797 or later.
        
        -----------------------------------------------------------------------------
        
end
  
-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local screenGroup = self.view
        local overlay_scene = event.sceneName  -- overlay scene name
 
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.797 or later.
        
        -----------------------------------------------------------------------------
        
end
 
 
 
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )
 
-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )
 
-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )
 
-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )
 
-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )
 
-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )
 
-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )
 
-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )
 
---------------------------------------------------------------------------------
 
return scene